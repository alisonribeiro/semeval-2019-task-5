# -*- coding: utf-8 -*-

#@author: alison

import sys
import itertools
import pandas as pd
import numpy as np
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt

def plot_confucion_matrix(cm, classes, title='Confusion matrix', normalize=False, cmap=plt.cm.Blues):
	if normalize:
		cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

	plt.imshow(cm, interpolation='nearest', cmap=cmap)
	plt.title(title)
	plt.colorbar()
	tick_marks = np.arange(len(classes))
	plt.xticks(tick_marks, classes, rotation=45)
	plt.yticks(tick_marks, classes)

	fmt = '.2f' if normalize else 'd'
	thresh = cm.max() / 2.
	for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
		plt.text(j, i, format(cm[i][j], fmt),
			horizontalalignment="center",
			color="white" if cm[i][j] > thresh else "black")

	plt.ylabel('True label')
	plt.xlabel('Predicted label')
	plt.tight_layout()

def main(task, file):

	if task == 'a' and file == 'en':
		#y_pred = pd.read_csv('en_a.tsv', delimiter='\t',encoding='utf-8')
		#y_test = pd.read_csv('labels_en.tsv', delimiter='\t',encoding='utf-8')
		y_p = pd.read_csv('CNN-FastText.tsv', delimiter='\t',encoding='utf-8')
		y_t = pd.read_csv('dev_en.tsv', delimiter='\t',encoding='utf-8')

		y_pred = ['hate' if k == 1 else 'non-hate' for k in y_p['HS']]
		y_test = ['hate' if k == 1 else 'non-hate' for k in y_t['HS']]

	elif task == 'b' and file == 'en':
		y_pred = pd.read_csv('en_b.tsv', delimiter='\t',encoding='utf-8')
		y_test = pd.read_csv('labels_en.tsv', delimiter='\t',encoding='utf-8')

	elif task == 'a' and file == 'es':
		y_pred = pd.read_csv('es_a.tsv', delimiter='\t',encoding='utf-8')
		y_test = pd.read_csv('labels_es.tsv', delimiter='\t',encoding='utf-8')

	elif task == 'b' and file == 'es':
		y_pred = pd.read_csv('es_a.tsv', delimiter='\t',encoding='utf-8')
		y_test = pd.read_csv('labels_es.tsv', delimiter='\t',encoding='utf-8')

	else:
		sys.exit('''
			wrong option
			''')

	cnf_matrix = confusion_matrix(y_test, y_pred)

	plt.figure()
	plot_confucion_matrix(cnf_matrix, classes=['hate','non-hate'], normalize=True, title='Confusion matrix', cmap=plt.cm.Blues)
	plt.show()

if __name__ == '__main__':

	args = sys.argv[1:]

	if len(args) >= 2:
		task = args[0]
		file = args[1]
		main(task, file)

	else:
		sys.exit('''
			Requires:
			option task -> for exemple: a (or b)
			option file -> for exemple: en (or es)
			''')